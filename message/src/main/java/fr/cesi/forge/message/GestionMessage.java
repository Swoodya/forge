package fr.cesi.forge.message;
import java.util.ResourceBundle; 

/** 
 * Voici mon projet d'informatique sous JAVA 13. je pr�sente solan�lement une agr�able lecture de mon code
 * @author Marco
 *
 */

public class GestionMessage {

	private static ResourceBundle bundle = ResourceBundle.getBundle("label");

	public static String getMessage( String key) {
		
		String retour = bundle.getString(key);
		
		return retour;
		
	}

}

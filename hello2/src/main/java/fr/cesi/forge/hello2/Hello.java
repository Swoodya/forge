package fr.cesi.forge.hello2;
import fr.cesi.forge.message.GestionMessage;
import fr.cesi.forge.hello2.GereMessage ;

public class Hello {

	public static void main(String[] args) {

		String bonjour = GestionMessage.getMessage("msg_hello");
		System.out.println(bonjour);
		String bonjours = GereMessage.gereMessage("msg");
		System.out.println(bonjours);
	}

}

package fr.cesi.forge.hello2 ;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import fr.cesi.forge.message.GestionMessage;

public class GereMessage {

	private static final Logger logger = LoggerFactory.getLogger(GereMessage.class);

	public  GereMessage(){
		logger.debug("GereMessage");
		logger.debug("GereMessage");
	}
	public static String gereMessage(String key){
		logger.debug("GereMessage");
		String msg = GestionMessage.getMessage(key);
		logger.debug("GereMessage");
		return msg;
	}
}
